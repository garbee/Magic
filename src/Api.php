<?php

namespace Garbee\Magic;

use InvalidArgumentException;
use Illuminate\Support\Collection;

final class Api
{
    /** @var string $version */
    private $version;

    /** @var Collection $availableSets */
    private $availableSets;

    public function __construct()
    {
        $this->version = json_decode(
            file_get_contents(__DIR__ . '/../data/version.json')
        );

        $this->availableSets = new Collection(json_decode(
            file_get_contents(__DIR__ . '/../data/SetList.json')
        ));
    }

    public function getCurrentVersion(): string
    {
        return $this->version;
    }

    public function getAvailableSets(): Collection
    {
        return $this->availableSets;
    }

    public function getAvailableSetCodes(): Collection
    {
        return $this->availableSets->map(function($item) {
            return $item->code;
        });
    }

    public function getAvailableSetNames(): Collection
    {
        return $this->availableSets->map(function($item) {
            return $item->name;
        });
    }

    /**
     * @param string $code
     *
     * @throws \InvalidArgumentException
     *
     * @return Set
     */
    public function getSet(string $code): Set
    {
        if (! $this->availableSets->pluck('code')->contains($code)) {
            throw new InvalidArgumentException(
                'An unsupported set code was provided.'
            );
        }

        $data = new Collection(json_decode(file_get_contents(
            __DIR__ . '/../data/AllSets-x.json'
        )));

        return Set::fromJsonObj($data->filter(function($item) use ($code) {
            return $item->code === $code;
        })->first());
    }

    public static function getAllSets() : Collection
    {
        return (new Collection(json_decode(file_get_contents(
            __DIR__ . '/../data/AllSets-x.json'
        ))))->map([Set::class, 'fromJsonObj']);
    }
}
