<?php

namespace Garbee\Magic;

use stdClass;
use Illuminate\Support\Collection;

final class Set
{
    /** @var string $name */
    public $name;

    /** @var string $code */
    public $code;

    /** @var string $gathererCode */
    public $gathererCode;

    /** @var string $oldCode */
    public $oldCode;

    /** @var string $magicCardsInfoCode */
    public $magicCardsInfoCode;

    /** @var string|null $releaseDate */
    public $releaseDate;

    /** @var string $border; */
    public $border;

    /** @var string $type */
    public $type;

    /** @var string|null $block */
    public $block;

    /** @var bool $onlineOnly */
    public $onlineOnly;

    /** @var array $booster */
    public $booster;

    /** @var Collection $cards */
    public $cards;

    public static function fromJsonObj(stdClass $obj): self
    {
        $self = new self();

        $self->name = $obj->name;
        $self->code = $obj->code;
        $self->gathererCode = $obj->gathererCode ?? null;
        $self->oldCode = $obj->oldCode ?? null;
        $self->magicCardsInfoCode = $obj->magicCardsInfoCode ?? null;
        $self->releaseDate = $obj->releaseDate ?? null;
        $self->border = $obj->border;
        $self->type = $obj->type;
        $self->block = $obj->block ?? null;
        $self->onlineOnly = $obj->onlineOnly ?? false;
        $self->booster = $obj->booster ?? null;
        $self->cards = collect($obj->cards)->map([Card::class, 'fromJsonObj']);

        return $self;
    }
}
