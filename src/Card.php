<?php

namespace Garbee\Magic;

use stdClass;

final class Card
{
    /** @var string $id */
    public $id;

    /** @var string $name */
    public $name;

    /** @var array|null $names */
    public $names;

    /** @var float $convertedManaCost */
    public $convertedManaCost;

    /** @var string $manaCost */
    public $manaCost;

    /** @var array|null $colors */
    public $colors;

    /** @var array|null $colorIdentity */
    public $colorIdentity;

    /** @var string $rulesText */
    public $rulesText;

    /** @var string $flavorText */
    public $flavorText;

    /** @var int $loyalty */
    public $loyalty;

    /** @var string $power */
    public $power;

    /** @var string $toughness */
    public $toughness;

    /** @var string $artist */
    public $artist;

    /** @var string $type */
    public $type;

    /** @var array|null $superTypes */
    public $superTypes;

    /** @var array|null $types */
    public $types;

    /** @var array|null $subtypes */
    public $subtypes;

    /** @var string $rarity */
    public $rarity;

    /** @var string $number */
    public $number;

    /** @var string $multiverseid */
    public $multiverseid;

    /** @var array|null $variations */
    public $variations;

    /** @var string $imageName */
    public $imageName;

    /** @var string $watermark */
    public $watermark;

    /** @var string $border */
    public $border;

    /** @var bool $timeShifted */
    public $timeShifted;

    /** @var int|null $hand */
    public $hand;

    /** @var int|null $life */
    public $life;

    /** @var bool $reserved */
    public $reserved;

    /** @var string|null $releaseDate */
    public $releaseDate;

    /** @var bool $starter */
    public $starter;

    /** @var string|null $mciNumber */
    public $mciNumber;

    public static function fromJsonObj(stdClass $obj): self
    {
        $self = new self();

        $self->id = $obj->id;
        $self->name = $obj->name;
        $self->names = $obj->names ?? null;
        $self->convertedManaCost = $obj->cmc ?? 0;
        $self->manaCost = $obj->manaCost ?? null;
        $self->colors = $obj->colors ?? null;
        $self->colorIdentity = $obj->colorIdentity ?? null;
        $self->rulesText = $obj->text ?? null;
        $self->flavorText = $obj->flavor ?? null;
        $self->loyalty = $obj->loyalty ?? null;
        $self->power = $obj->power ?? null;
        $self->toughness = $obj->toughness ?? null;
        $self->artist = $obj->artist ?? null;
        $self->type = $obj->type;
        $self->superTypes = $obj->superTypes ?? null;
        $self->types = $obj->types ?? null;
        $self->subtypes = $obj->subtypes ?? null;
        $self->rarity = $obj->rarity;
        $self->number = $obj->number ?? null;
        $self->multiverseid = $obj->multiverseid ?? null;
        $self->variations = $obj->variations ?? null;
        $self->imageName = $obj->imageName;
        $self->watermark = $obj->watermark ?? null;
        $self->border = $obj->border ?? null;
        $self->timeShifted = $obj->timeShifted ?? false;
        $self->hand = $obj->hand ?? null;
        $self->life = $obj->life ?? null;
        $self->reserved = $obj->reserved ?? false;
        $self->releaseDate = $obj->releaseDate ?? null;
        $self->starter = $obj->starter ?? false;
        $self->mciNumber = $obj->mciNumber ?? null;

        return $self;
    }
}
