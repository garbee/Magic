<?php

namespace Garbee\Magic\Tests;

use Garbee\Magic\Api;
use PHPUnit\Framework\TestCase;

class Test extends TestCase
{
    /** @var Api $api */
    private $api;

    protected function setUp()
    {
        $this->api = new Api();
    }

    public function testPullingSets()
    {
        $this->api->getAvailableSets()
            ->random(5)
            ->each(function (\stdClass $set) {
                $this->assertSame(
                    $set->name,
                    $this->api->getSet($set->code)->name
                );
            });
    }

    public function testAllSets()
    {
        $sets = Api::getAllSets();

        $this->assertCount(count($this->api->getAvailableSetCodes()), $sets);
    }

    public function testGettingNonExistantSetFails()
    {
        $this->expectException(\InvalidArgumentException::class);

        $this->api->getSet('WHATEVER');
    }

    public function testCanGetDataVersion()
    {
        $this->assertGreaterThanOrEqual(
            '3.8.2',
            $this->api->getCurrentVersion()
        );
    }

    public function testCanGetSetNames()
    {
        $this->assertTrue(
            $this->api->getAvailableSetNames()->contains('Aether Revolt')
        );
    }
}
